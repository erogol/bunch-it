class CreateQuerycountries < ActiveRecord::Migration
  def change
    create_table :querycountries do |t|
      t.string :country_code, :limit=>4
      t.integer :enter_count
      t.integer :query_id

      t.timestamps
    end
  end
end
