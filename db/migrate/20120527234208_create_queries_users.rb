class CreateQueriesUsers < ActiveRecord::Migration
  def change
    create_table :queries_users do |t|
      t.integer :user_id
      t.integer :query_id

      t.timestamps
    end
  end
end
