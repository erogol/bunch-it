SampleApp::Application.routes.draw do
  devise_for :users,:controllers => {:registrations => "registrations"}
  resources :home, :only => :index
  resources :users
  
  get "pages/contact"
  get "pages/about"
  get "pages/help"

  get "results/result"
  get "results/wiki_results"
  get "results/job_results"
  get "results/image_results"
  get "results/news_results"
  
  post "deleteFolder" => 'tree#deleteFolder'
  post "insertResult" => 'tree#insertResult'
  post "createFolder" => 'tree#createFolder'
  post "deleteResult" => 'tree#deleteResult'
  
  get "mapLoc" => 'map#getLocationMap'
  get "weather"=> 'map#getWeatherMap'
  get "address" => 'map#getAddressMap'

  match '/contact' , :to => 'pages#contact'
  match '/about' , :to => 'pages#about'
  match '/help' , :to => 'pages#help'
  match '/bubble', :to => 'home#bubbleAnswer'
  
  match '/web' , :to => 'results#result'
  match '/wiki' , :to => 'results#wiki_results'
  match '/job' , :to => 'results#job_results'
  match '/news' , :to => 'results#news_results'
  match '/img' , :to => 'results#image_results'
  
  match '/webh' , :to=> 'home#index'
  match '/wikih', :to=> 'home#wiki_index'
  match '/imgh', :to=> 'home#img_index'
  match '/newsh', :to=> 'home#news_index'
  match '/jobh', :to=> 'home#job_index'
  #match '/home_index', :to => 'pages#home'
  #match '/signin', :to => 'sessions#new'
  #match '/signout', :to => 'sessions#destroy'
  root :to => 'home#index'

  devise_scope :user do
    get '/login' => 'devise/sessions#new'
    get '/logout' => 'devise/sessions#destroy'
    
  end
 

# The priority is based upon order of creation:
# first created -> highest priority.

# Sample of regular route:
#   match 'products/:id' => 'catalog#view'
# Keep in mind you can assign values other than :controller and :action

# Sample of named route:
#   match 'products/:id/purchase' => 'catalog#purchase', :as => :purchase
# This route can be invoked with purchase_url(:id => product.id)

# Sample resource route (maps HTTP verbs to controller actions automatically):
#   resources :products

# Sample resource route with options:
#   resources :products do
#     member do
#       get 'short'
#       post 'toggle'
#     end
#
#     collection do
#       get 'sold'
#     end
#   end

# Sample resource route with sub-resources:
#   resources :products do
#     resources :comments, :sales
#     resource :seller
#   end

# Sample resource route with more complex sub-resources
#   resources :products do
#     resources :comments
#     resources :sales do
#       get 'recent', :on => :collection
#     end
#   end

# Sample resource route within a namespace:
#   namespace :admin do
#     # Directs /admin/products/* to Admin::ProductsController
#     # (app/controllers/admin/products_controller.rb)
#     resources :products
#   end

# You can have the root of your site routed with "root"
# just remember to delete public/index.html.
# root :to => 'welcome#index'

# See how all your routes lay out with "rake routes"

# This is a legacy wild controller route that's not recommended for RESTful applications.
# Note: This route will make all actions in every controller accessible via GET requests.
# match ':controller(/:action(/:id))(.:format)'
end
