module ResultsHelper
  def highlight_keyword(text,keyword)
    keywords = keyword.split
    keywords.each do |s| 
      puts s
      matcher = /(( +)|^)#{Regexp.quote(s)}(( +)|$)/i
      text.gsub!(matcher, ' <strong class="highlight">\&</strong> ')
   end
   text.html_safe()
  # Based on ActionView::Helpers::TextHelper#highlight
  end
end
