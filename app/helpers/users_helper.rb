module UsersHelper
  def gravatar_for(user, options = { :size => 80 })
    gravatar_image_tag(user.email.downcase, :alt => user.user_name,
                                            :class => 'gravatar round',
                                            :gravatar => options)
  end
  
  def devise_error_messages!
    return "" if resource.errors.empty?
    flash.now[:alert] = "<li>".html_safe+resource.errors.full_messages.join("<li>").html_safe;
  end
end
