class FeedbackSender < ActionMailer::Base
  default :from => "feedback@gmail.com"
  default :to => "bunchitservice@gmail.com"
  def sent(sent_at = Time.now)
    @subject= 'OrderMailer#confirm'
    @body= {}
    @recipients = ''
    @from= ''
    @sent_on= sent_at
    @headers= {}
  end

  def new_message(message)
    @message = message
    @message[:subject]="Feedback"
    mail(:subject => "#{message[:subject]}")
  end
end
