class Querycountry < ActiveRecord::Base
  attr_accessible :id, :country_code, :enter_count, :query_id
  belongs_to :query
end
