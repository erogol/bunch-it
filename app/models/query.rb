class Query < ActiveRecord::Base
  attr_accessible :query_string, :country_code, :enter_count, :user_id
  has_many :querycountries

  has_many                            :queries_users
  has_many :users, :through =>        :queries_users

  validates :query_string,  :presence => true,
                     :length => { :within => 1..140 }
end
