var fill;

var w ;
var h ;

var scaleSettings = {"def":{"from":-90, "to":90, "count":20},"1":{"from":-90, "to":90, "count":3},"2":{"from":-90, "to":90, "count":1},"3":{"from":0 , "to":0, "count":1}};

var words,
    max,
    scale,
    tagScale,
    complete,
    keyword,
    tags,
    fontSize,
    maxLength,
    maxScore,
    minScore;

var layout;

var svg;
    
var background,
	vis;


function initCloud(){
	fill = d3.scale.category20b();
	w = 600;
	h = 580;
	//scaleSettings = {"def":{"from":-90, "to":90, "count":20},"1":{"from":-90, "to":90, "count":3},"2":{"from":-90, "to":90, "count":1},"3":{"from":0 , "to":0, "count":1}};
	tagScale = d3.scale.linear();
	words = [];
    scale = 1;
    complete = 0;
    keyword = "";
    maxLength = 30;
    fetcher = "http://search.twitter.com/search.json?rpp=100&q={word}";
    maxScore = 0;
    minScore = 9000;
    layout = d3.layout.cloud();
    
	parseJson();
	fontSize = d3.scale.linear().domain([minScore/2,maxScore]).range([10, 50]);
	svg = d3.select("#vis").append("svg")
			    .attr("width", w)
			    .attr("height", h);   
			    
    background = svg.append("g"),
    
    vis = svg.append("g")
   				.attr("transform", "translate(" + [w >> 1, h >> 1] + ")");  
	
	getSetting("def");
	
	layout.timeInterval(10)
			    .size([w, h])
			    .words(tags)
			    .fontSize(function(d) { return fontSize(d.size); })
			    .text(function(d) { return d.text; })
			    .on("word", function(d){ })
			    .on("end", draw)	
			    .start();
	
	d3.select("#download-png").on("click", downloadPNG);		    
}

function initCloudWithSetting(settingNum){
	fill = d3.scale.category20b();
	w = 600;
	h = 580;
	tagScale = d3.scale.linear();
	words = [];
    scale = 1;
    complete = 0;
    keyword = "";
    maxLength = 30;
    fetcher = "http://search.twitter.com/search.json?rpp=100&q={word}";
    maxScore = 0;
    minScore = 9000;
    layout = d3.layout.cloud();
    
	parseJson();
	fontSize = d3.scale.linear().domain([minScore/2,maxScore]).range([10, 50]);
	svg = d3.select("#vis").append("svg")
			    .attr("width", w)
			    .attr("height", h);   
			    
    background = svg.append("g"),
    
    vis = svg.append("g")
   				.attr("transform", "translate(" + [w >> 1, h >> 1] + ")");  
	
	getSetting(settingNum);
	
	layout.timeInterval(10)
			    .size([w, h])
			    .words(tags)
			    .data(tags)
			    .fontSize(function(d) { return fontSize(d.size); })
			    .text(function(d) { return d.text; })
			    .on("word", function(d){})
			    .on("end", draw)	
			    .start();
	
	d3.select("#download-png").on("click", downloadPNG);		    
}

function updateTagCloudSetting(settingNum){
	layout.stop().words(tags.slice(0,tags.length)).start();
	getSetting(settingNum);
}

function parseJson(){
	tags=new Array;
	var score;
	var text;
	for(i = 0; i<json.clusters.length; i++){
		score = json.clusters[i].score;
		text = json.clusters[i].phrases[0];
		documents = json.clusters[i].documents;
		id = json.clusters[i].id;
		//it it is "other Topic"
		if(i+1 == json.clusters.length){score = maxScore/2;}
		if(text.length > 20){text = text.substring(0,20)+"...";}
		tags.push({text:text, size:score	, id:id});
		//set max min score
		if(score < minScore) {minScore = score;}
		if(score > maxScore) {maxScore = score;}
	}
}

function getSetting(index) {
	var setting = scaleSettings[index+""];
    count = setting.count;
    from = Math.max(-90, Math.min(90, setting.from));
    to = Math.max(-90, Math.min(90, setting.to));
    tagScale.domain([0, count - 1]).range([from, to]);
    var step = (to - from) / count;
    layout.rotate(function() {
			return tagScale(~~(Math.random() * count));
	});
    //update();
 }

function draw(data, bounds) {
	scale = bounds ? Math.min(
      w / Math.abs(bounds[1].x - w / 2),
      w / Math.abs(bounds[0].x - w / 2),
      h / Math.abs(bounds[1].y - h / 2),
      h / Math.abs(bounds[0].y - h / 2)) / 2 : 1;
	words = data;
	var text = vis.selectAll("text")
				.data(words)
		
		text.transition()
      			.duration(1000)
      			.attr("transform", function(d) { return "translate(" + [d.x, d.y] + ")rotate(" + d.rotate + ")"; })
      			.style("font-size", function(d) { return d.size + "px"; });
      			
		text.enter().append("text")
		        .attr("text-anchor", "middle")
		        .attr("transform", function(d) { return "translate(" + [d.x, d.y] + ")rotate(" + d.rotate + ")"; })
		        .style("font-size", function(d) { return d.size + "px"; })
		        .on("click", function(d) {
		           onClickTag(d);
		        })
		        .style("opacity", 1e-6)
		        .transition()
		        .duration(1000)
		        .style("opacity", 1);
		        
        text.style("cursor","pointer")
	      		.style("fill", function(d) { return fill(d.text.toLowerCase()); })
	     	    .text(function(d) { return d.text; });
	    var exitGroup = background.append("g")
	  			.attr("transform", vis.attr("transform"));
	    var exitGroupNode = exitGroup.node();
			    text.exit().each(function() {
			      exitGroupNode.appendChild(this);
			    });
	  	exitGroup.transition()
	      		.duration(1000)
	      		.style("opacity", 1e-6)
	      		.remove();
	  	vis.transition()
	      		.delay(1000)
	      		.duration(750)
	      		.attr("transform", "translate(" + [w >> 1, h >> 1] + ")scale(" + scale + ")");
}

// Converts a given word cloud to image/png.
function downloadPNG() {
  var canvas 	= document.createElement("canvas"),
      c 		= canvas.getContext("2d");
  canvas.width 	= w;
  canvas.height = h;
  c.translate(w >> 1, h >> 1);
  c.scale(scale, scale);
  words.forEach(function(word, i) {
    c.save();
    c.translate(word.x, word.y);
    c.rotate(word.rotate * Math.PI / 180);
    c.textAlign = "center";
    c.fillStyle = fill(word.text.toLowerCase());
    c.font = word.size + "px " + word.font;
    c.fillText(word.text, 0, 0);
    c.restore();
  });
  //d3.select(this).attr("href", canvas.toDataURL("image/png"));
  window.open(canvas.toDataURL("image/png"));
}

function onClickTag(d){
	updateResults(json.clusters[d.id].documents);
}

