/*
 * Functions is called with d3.json callback
 * It finds the overlapping documents in the clusters and keeps the stat on the links 2d array (matrix)
 */

function clone(obj) {
    if (null == obj || "object" != typeof obj) return obj;
    var copy = obj.constructor();
    for (var attr in obj) {
        if (obj.hasOwnProperty(attr)) copy[attr] = obj[attr];
    }
    return copy;
}

var maxOverlap = 0;

function FindOverlappingDocs(data, numOfCluster) {
	var nodes = new Array(numOfCluster);
	var overlap = new Array(numOfCluster);
	var totalOverlapLink = 0;
	for( i = 0; i < numOfCluster; i++) {
		overlap[i] = new Array(numOfCluster);
		for( j = 0; j < numOfCluster; j++) {
			overlap[i][j] = 0;
		}
	}
	for( i = 0; i < numOfCluster; i++) {
		for( k = 0; k < data.clusters[i].documents.length; k++) {
			var docNo = data.clusters[i].documents[k];
			for( l = i + 1; l < numOfCluster; l++) {
				var relScore = overlap[i][l];
				for( z = 0; z < data.clusters[l].documents.length; z++) {
					if(docNo == data.clusters[l].documents[z]) {
						relScore++;
					}
				}
				if(relScore > 0 && overlap[i][l] == 0){
					totalOverlapLink++;
				}
				overlap[i][l] = relScore;
			}
		}
	}
	
	//console.log(totalOverlapLink);
	
	var links = new Array(totalOverlapLink);
	var temp = {
		"source" : "",
		"target" : "",
		"overlapNum" : ""
	};
	
	for( counter = 0,i = 0; i < numOfCluster; i++){ 
		for( l = i; l < numOfCluster; l++) {
			if(overlap[i][l] > 0){
				temp.source = i;
				temp.target = l;
				temp.overlapNum = overlap[i][l];
				if(maxOverlap<overlap[i][l])
					maxOverlap = overlap[i][l];
				//console.log(temp);
				links[counter] = clone(temp);
				counter++
			}
		}
	}

	return links;
}


function getDomainUrlOfAddress(url){
	return (url.match(/:\/\/(.[^/]+)/)[1]).replace('www.','');
}

//split the anchors to the array to be returned
function getAnchors(location){
	anchors = location.split("#");
	anchors.splice(0,1);
	for (i = 0; i < anchors.length; i++){
		//if it is setting0 anchor
		if(anchors[i].indexOf("a") > -1){
			setting_0_anchors.push(anchors[i].substring(1));
		}
		//if it is setting1 anchor
		if(anchors[i].indexOf("b") > -1){
			setting_1_anchors.push(anchors[i].substring(1));
		}
	}
	console.log(setting_0_anchors);
	return anchors;
}

//set the graph map according to the anchors
function setGraphMapWithAnchor(){
	//set coloring
	console.log(setting_0_anchors);
	if(settings == 0){
		d3.selectAll("circle")
		.style("fill",function(d,i){
			var index = setting_0_anchors.indexOf(d.id+'');
			//console.log(setting_0_anchors);
			if(index> -1){
				//if it is lastly selected one
				if(index == setting_0_anchors.length-1){
					return "lightcoral";
				}else{
				//if it is selected one
					return "lightgreen";
				}
			}
			//if it is not selected
			return "lightblue"
		})
		.style("stroke", function(d,i){
			var index = setting_0_anchors.indexOf(d.id+'');
			if(index > -1){
				//if it is lastly selected one
				if(index == setting_0_anchors.length-1){
					lastSelectedNode = this;
					updateResults(d.docs);
					return "red";
				}else{
					//if it is selected one
					return "green";
				}
			}
			//if it is not selected
			return "blue";
			});
	}else if(settings == 1){
		d3.selectAll("circle")
		.style("fill",function(d,i){
			var index = setting_1_anchors.indexOf(d.id+'');
			if(index> -1){
				//if it is lastly selected one
				if(index == setting_1_anchors.length-1){
					return "lightcoral";
				}else{
				//if it is selected one
					return "lightgreen";
				}
			}
			//if it is not selected
			return "lightblue"
		})
		.style("stroke", function(d,i){
			var index = setting_1_anchors.indexOf(d.id+'');
			if(index > -1){
				//if it is lastly selected one
				if(index == setting_1_anchors.length-1){
					lastSelectedNode = this;
					updateResultsSetting1(findMatchingDocsWithTheSite(d.url, json.documents.length));
					return "red";
				}else{
					//if it is selected one
					return "green";
				}
			}
			//if it is not selected
			return "blue";
			});
	}
}

function setNextUrlAnchor(id){
	//control for the anchor, is it in already
	if(anchors.indexOf(id+"")>-1){
		return;
	}else{
		//if current setting is content clustering
		if(settings == 0){
			anchors.push("a"+id);
			setting_0_anchors.push(""+id);
			window.location = window.location+"#a"+id;
		}else if(settings == 1){//else current setting is website clustering
			anchors.push("b"+id);
			setting_1_anchors.push(""+id);
			window.location = window.location+"#b"+id;
		}
		
	//if it is not insert it to url
	}
}

//text highlighting 
function highlight(){
	var strings = query.split(" ");
	for(i = 0; i < query.length; i++){
		$('.resultsDiv').highlight(new RegExp(" +"+strings[i]+" +","ig"));
	}
}


