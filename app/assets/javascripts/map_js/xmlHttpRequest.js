//Author: Eren Golge
//Updated: 14-01-2012 05:20 AM - added GoogleWeatherAPI functions

var xml;

//weather API variables
var xml2;
//weatherAPI
var latitude;
//for weather API
var longitude;
//for weather API Google weather API uses 7-8 without point langitude and latitude values
var imageURL;
var tomorrow = new Array();
// 0 - low 	1 - high 	2 - condition 	3 - icon

function formatting(lat) {
	var lat2 = lat + "";
	while(lat2.length < 10) {
		lat2 = lat2.concat('0');
	}
	lat2 = lat2.indexOf('-') !== -1 ? lat2.substr(0, 10) : lat2.substring(0, 9);
	return lat2.replace('.', '');
}

function loadWeatherXML(lat, lon) {
	var file = "/weather?lat="+lat + "&lon=" + lon;
	if(window.XMLHttpRequest) {// code for IE7+, Firefox, Chrome, Opera, Safari
		xmlhttp = new XMLHttpRequest();
	} else {// code for IE6, IE5
		xmlhttp = new ActiveXObject("Microsoft.XMLHTTP");
	}
	xmlhttp.onreadystatechange = function() {
		if(xmlhttp.readyState == 4 && xmlhttp.status == 200) {
			xml2 = xmlhttp.responseText;
			//alert(xml);
		}
	}
	xmlhttp.open("GET", file, false);
	xmlhttp.send();
}

function loadXMLDoc(lon, lat) {
	var file = "/mapLoc?lat="+ lat + "&lon=" + lon ;
	if(window.XMLHttpRequest) {// code for IE7+, Firefox, Chrome, Opera, Safari
		xmlhttp = new XMLHttpRequest();
	} else {// code for IE6, IE5
		xmlhttp = new ActiveXObject("Microsoft.XMLHTTP");
	}
	xmlhttp.onreadystatechange = function() {
		if(xmlhttp.readyState == 4 && xmlhttp.status == 200) {
			xml = xmlhttp.responseText;
		}
	}
	xmlhttp.open("GET", file, false);
	xmlhttp.send();
	;
}

function createPopup(text, lon, lat, tempC, tempF, imgW, tomor) {
	try {
		var lonNonTrandformed = lon;
		var latNonTransformed = lat;
		var lonlat = new OpenLayers.LonLat(lon, lat).transform(new OpenLayers.Projection("EPSG:4326"), map.getProjectionObject());

		if(tempC != null) {
			var tomorrowCLow = ((5 / 9) * (parseInt(tomor[0]) - 32)) + "";
			tomorrowCLow = tomorrowCLow.substr(0, 4);
			var tomorrowCHigh = ((5 / 9) * (parseInt(tomor[1]) - 32)) + "";
			tomorrowCHigh = tomorrowCHigh.substr(0, 4);
			popup = new OpenLayers.Popup.FramedCloud("address", lonlat, null, "<div style='font-size:14px'>" + "<font color='#FF0000'><img src='assets/map_images/Images/Coordinates.jpg'/></font>" + lonNonTrandformed + " E, " + latNonTransformed + " N, <br/>" + "<img src='assets/map_images/Images/Address.jpg'/>" + text + "<br/>" + '<dd><img class="round" alt="no-img" style="vertical-align:middle;" src="http://www.google.com' + imgW + '"/>  ' + tempC + "&#8451;" + "  " + tempF + "&#8457; <br/>" + '<dd>Tomorrow: <img class="round" alt="no-img" style="vertical-align:middle;" src="http://www.google.com' + tomor[3] + '"/>  low:' + tomorrowCLow + "&#8451;-" + tomor[0] + "&#8457; high: " + tomorrowCHigh + "&#8451;-" + tomor[1] + "&#8457;" + "</div>", null, true);
		} else {
			popup = new OpenLayers.Popup.FramedCloud("address", lonlat, null, "<div style='font-size:14px'>" + "<img src='assets/map_images/Images/Coordinates.jpg'/>" + lonNonTrandformed + " E, " + latNonTransformed + " N, <br/>" + "<img src='assets/map_images/Images/Address.jpg'/>" + text + "<br/>", null, true);
		}
		map.addPopup(popup);
	} catch(err) {
		alert(err);
	}
}

function parseXML(lon, lat, isUserLocation) {
	try {
		longitude = formatting(lon);
		latitude = formatting(lat);
		loadWeatherXML(latitude, longitude);
		loadXMLDoc(lon, lat);
		var tempratureC;
		var tempratureF;

		if(xml != null) {
			//alert(xml);
			xml = (new DOMParser()).parseFromString(xml, "text/xml");
			path = '/GeocodeResponse/result/formatted_address[1]/text()';

			// code for IE
			if(window.ActiveXObject) {
				var nodes = xml.selectNodes(path);

				//alert("fsdgsdg"+nodes[0].nodeValue);
				document.write(nodes[0].nodeValue);
				document.write("<br />");

			}
			// code for Mozilla, Firefox, Opera, etc.
			else if(document.implementation && document.implementation.createDocument) {
				var nodes = xml.evaluate(path, xml, null, XPathResult.ANY_TYPE, null);
				var result = nodes.iterateNext();
			}
			if(xml2 != null) {
				tempratureC = $(xml2).find("temp_c").attr('data');
				tempratureF = $(xml2).find("temp_f").attr('data');
				imageURL = $(xml2).find("icon").attr('data');
				tomorrow[0] = $(xml2).find("low").attr('data');
				tomorrow[1] = $(xml2).find("high").attr('data');
				tomorrow[2] = $(xml2).find("condition").attr('data');
				tomorrow[3] = $(xml2).find('icon:eq(2)').attr('data');
			} else {
				tempratureC = null;
			}
		}
		if(result == null) {
			createPopup("Address cannot be found!", lon, lat);
		} else {
			if(isUserLocation) {
				//alert("Address: "+result.nodeValue);
				createPopup(result.nodeValue + " (is yours current address.)", lon, lat, tempratureC, tempratureF, imageURL, tomorrow);
			} else {
				createPopup(result.nodeValue, lon, lat, tempratureC, tempratureF, imageURL, tomorrow);
			}
		}
	} catch(err) {
		alert(err);
	}
}

function loadAddressXML(address) {
	var file = "/address?address="+address;
	if(window.XMLHttpRequest) {// code for IE7+, Firefox, Chrome, Opera, Safari
		xmlhttp = new XMLHttpRequest();
	} else {// code for IE6, IE5
		xmlhttp = new ActiveXObject("Microsoft.XMLHTTP");
	}
	xmlhttp.onreadystatechange = function() {
		if(xmlhttp.readyState == 4 && xmlhttp.status == 200) {
			address_json = xmlhttp.responseText;
			parseAddressJson();
		}
	}
	xmlhttp.open("GET", file, false);
	xmlhttp.send();
}

function parseAddressJson() {
	address_json = eval("(" + address_json + ")");
	//alert(address_json.results[0].address_components[0].long_name);/*************/
	var max_lat = -10000;
	var max_long = -100000;
	var min_lat = 100000;
	var min_long = 100000;
	var size = new OpenLayers.Size(21, 25);
	var offset = new OpenLayers.Pixel(-(size.w / 2), -size.h);
	var icon = new OpenLayers.Icon('http://www.openlayers.org/dev/img/marker.png', size, offset);

	for(var i = 0; i < map.popups.length; ++i) {
		map.removePopup(map.popups[i]);
	}

	for(var i = 0; i < address_json.results.length; i++) {
		var temp = address_json.results[i].geometry.location.lat;
		var temp2 = address_json.results[i].geometry.location.lng;
		var marker = new OpenLayers.Marker(new OpenLayers.LonLat(temp2, temp).transform(new OpenLayers.Projection("EPSG:4326"), map.getProjectionObject()), icon.clone());
		var feature = new OpenLayers.Feature(markers, new OpenLayers.LonLat(temp2, temp).transform(new OpenLayers.Projection("EPSG:4326"), map.getProjectionObject()));
		feature.popupClass = OpenLayers.Class(OpenLayers.Popup.FramedCloud, {
			minSize : new OpenLayers.Size(0, 0)
		});
		feature.closeBox = true;
		feature.data.popupContentHTML = address_json.results[i].formatted_address;
		feature.data.overflow = "hidden";
		marker.feature = feature;
		var markerClick = function(evt) {
			for(var i = 0; i < map.popups.length; ++i) {
				map.removePopup(map.popups[i]);
			}
			this.popup = this.createPopup(this.closeBox);
			map.addPopup(this.popup);
			this.popup.show();
			OpenLayers.Event.stop(evt);
		};
		marker.events.register("mouseover", feature, markerClick);
		markers.addMarker(marker);

		if(max_lat < temp)
			max_lat = temp;
		if(min_lat > temp)
			min_lat = temp;
		if(max_long < temp2)
			max_long = temp2;
		if(min_long > temp2)
			min_long = temp2;
	}
	bounds = new OpenLayers.Bounds(min_long, min_lat, max_long, max_lat);
	bounds.transform(new OpenLayers.Projection("EPSG:4326"), map.getProjectionObject());
	map.zoomToExtent(bounds, false);
	map.setCenter(bounds);
}

// JavaScript Document