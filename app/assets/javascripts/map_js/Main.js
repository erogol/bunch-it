//To collect the lon lat from mouse click
	var clickedLon;
	var clickedLat;
    var map;
    var markers;
	
	
	//Create Click controll
	OpenLayers.Control.Click = OpenLayers.Class(OpenLayers.Control,{
		defaultHandlerOption:{
			'single':true,
			'double':false,
			'pixelTolerance': 0,
			'stopSingle': false,
			'stopDouble': false
		},
		
		initialize: function(options) {
			this.handlerOptions = OpenLayers.Util.extend(
				{}, this.defaultHandlerOptions
			);
			OpenLayers.Control.prototype.initialize.apply(
				this, arguments
			); 
			this.handler = new OpenLayers.Handler.Click(
				this, {
					'click': this.trigger
				}, this.handlerOptions
			);
		}, 
				
				 trigger: function(e) {
                  var lonlat = map.getLonLatFromViewPortPx(e.xy).transform(
						map.getProjectionObject(),        				
        				new OpenLayers.Projection("EPSG:4326")
          			);
						clickedLon = lonlat.lon;
						clickedLat = lonlat.lat;
						parseXML(clickedLon, clickedLat);				  
                }
            });					

 		
		//initialize the map
        function init() {
			//add controls
			map = new OpenLayers.Map('map');
         	map.addControl(new OpenLayers.Control.LayerSwitcher());
         	map.addControl(new OpenLayers.Control.MousePosition());   
			map.removeControl(map.getControl(0));
		   	var clicked = new OpenLayers.Control.Click();
		   	map.addControl(clicked);
			clicked.activate();
			
			
				//add maps
            
            

	        // API key for http://openlayers.org. Please get your own at
	        // http://bingmapsportal.com/ and use that instead.
	        var apiKey = "AiMb-PNpXiLRklKjHYZ6D_kczuZKp4piQPVih8BzLnLc_yQOeEhPl-Fve7KXsnFh";
	  
            var road = new OpenLayers.Layer.Bing({
                name: "Bing Road",
                key: apiKey,
                type: "Road"
            });
            var hybrid = new OpenLayers.Layer.Bing({
                name: "Bing Hybrid",
                key: apiKey,
                type: "AerialWithLabels"
            });
            var aerial = new OpenLayers.Layer.Bing({
                name: "Bing Aerial",
                key: apiKey,
                type: "Aerial"
            });

    
            var gphy = new OpenLayers.Layer.Google(
                "Google Physical",
                {type: google.maps.MapTypeId.TERRAIN}
            );
            var gmap = new OpenLayers.Layer.Google(
                "Google Streets", // the default
                {numZoomLevels: 20}
            );
            var ghyb = new OpenLayers.Layer.Google(
                "Google Hybrid",
                {type: google.maps.MapTypeId.HYBRID, numZoomLevels: 20}
            );
            var gsat = new OpenLayers.Layer.Google(
                "Google Satellite",
                {type:  google.maps.MapTypeId.SATELLITE, numZoomLevels: 22}
            );
		
 
            map.addLayers([road, hybrid, aerial, gmap, gphy, ghyb, gsat]);
            markers = new OpenLayers.Layer.Markers( "Markers" );
				map.addLayer(markers);
				map.setCenter(new OpenLayers.LonLat(32.54, 39.57).transform(
        			new OpenLayers.Projection("EPSG:4326"),
        			map.getProjectionObject()
    			), 5);
				
				if(navigator.geolocation){
					setTimeout(getCurrentLoc,500);//to get correct coordinates
				}else{
					alert("GeoLocation is not supported");
				}
			
			
			//use geolocation option of browser and find the current address of user
			function getCurrentLoc(){
				try{					
					navigator.geolocation.getCurrentPosition(
						function(position){						
							var co = new OpenLayers.LonLat(position.coords.longitude, position.coords.latitude);											  					
							var coNotTransformed = new OpenLayers.LonLat(position.coords.longitude, position.coords.latitude);   						
   						co.transform(
        					new OpenLayers.Projection("EPSG:4326"),
        					map.getProjectionObject()
   						);  					   						
   						map.setCenter(co,5);
							var icon = new OpenLayers.Icon("assets/map_images/Images/icon.gif",null,null);
							markers.addMarker(new OpenLayers.Marker(co,icon));
							parseXML(coNotTransformed.lon, coNotTransformed.lat, 1);		
						}
					);
				}catch(err){
					alert(err);
				}
			}				
        }
        
        function refreshMap(){
        		if(map)
   			{
       			map.destroy();
        			map = null;
    			}
				init();
			}