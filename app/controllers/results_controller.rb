require 'carrot2'

class ResultsController < ApplicationController
  layout "_result"
  def result
    @source_setting = "web"
    initResult
    #insert query to DB
    unless(@query.nil?)
      insertQueryToDB(@query)
      session[:query] = @query
    else
      @query = session[:query];
    end

    puts "\n## Clustering data from a search engine...\n"
    carrot = Carrot2.new
    uri = "http://localhost:8080/dcs/rest";
    @response = carrot.dcs_request(uri, {
       "dcs.source" => "etools",
       "query" => @query,
       "dcs.output.format" => "JSON",
       "dcs.clusters.only" => "false",
       "results"=>100
      })
    @json = @response
    @response = JSON.parse(@response)
    #@json = @response['clusters']
    #@json = JSON.generate(@json)
    if @response['clusters'].size == 0
      flash.now[:notice] = "No cluster generated (Insufficient number of results)! "
      puts "No cluster could be generated! <Sufficient no. of result>!"
    end
    carrot.dump(@response)
    if @cluster != nil
      @clusterDocs  = @response['clusters'][@cluster]['documents']
    end
  end

  def wiki_results
    @source_setting = "wiki"
    initResult
    #insert query to DB
    unless(@query.nil?)
      insertQueryToDB(@query)
      session[:query] = @query
      params[:query] = @query
    else
      @query = session[:query];
      params[:query] = session[:query];
    end
    puts "\n## Clustering data from a search engine...\n"
    carrot = Carrot2.new
    uri = "http://localhost:8080/dcs/rest";
    @response = carrot.dcs_request(uri, {
       "dcs.source" => "wiki",
       "query" => @query,
       "dcs.output.format" => "JSON",
       "dcs.clusters.only" => "false",
      })

    @json = @response
    @response = JSON.parse(@response)
    #@json = @response['clusters']
    #@json = JSON.generate(@json)
    if @response['clusters'].size == 0
      flash.now[:notice] = "No cluster generated (Insufficient number of results)! "
      puts "No cluster could be generated! <Sufficient no. of result>!"
    end
    carrot.dump(@response)
    if @cluster != nil
      @clusterDocs  = @response['clusters'][@cluster]['documents']
    end
  end

  def job_results
    @source_setting = "job"
    initResult
    #insert query to DB
    unless(@query.nil?)
      insertQueryToDB(@query)
      session[:query] = @query
    else
      @query = session[:query];
    end
    puts "\n## Clustering data from a search engine...\n"
    carrot = Carrot2.new
    uri = "http://localhost:8080/dcs/rest";
    @response = carrot.dcs_request(uri, {
       "dcs.source" => "indeed",
       "query" => @query,
       "dcs.output.format" => "JSON",
       "dcs.clusters.only" => "false"
      })

    @json = @response
    @response = JSON.parse(@response)
    #@json = @response['clusters']
    #@json = JSON.generate(@json)
    if @response['clusters'].size == 0
      flash.now[:notice] = "No cluster generated (Insufficient number of results)! "
      puts "No cluster could be generated! <Sufficient no. of result>!"
    end
    carrot.dump(@response)
    if @cluster != nil
      @clusterDocs  = @response['clusters'][@cluster]['documents']
    end
  end

  def news_results
    @source_setting = "news"
    initResult
    #insert query to DB
    unless(@query.nil?)
      insertQueryToDB(@query)
      session[:query] = @query
    else
      @query = session[:query];
    end
    puts "\n## Clustering data from a search engine...\n"
    carrot = Carrot2.new
    uri = "http://localhost:8080/dcs/rest";
    @response = carrot.dcs_request(uri, {
       "dcs.source" => "news",
       "query" => @query,
       "dcs.output.format" => "JSON",
       "dcs.clusters.only" => "false"
      })

    @json = @response
    @response = JSON.parse(@response)
    #@json = @response['clusters']
    #@json = JSON.generate(@json)
    if @response['clusters'].size == 0
      flash.now[:notice] = "No cluster generated (Insufficient number of results)! "
      puts "No cluster could be generated! <Sufficient no. of result>!"
    end
    carrot.dump(@response)
    if @cluster != nil
      @clusterDocs  = @response['clusters'][@cluster]['documents']
    end
  end

  def image_results
    @source_setting = "img"
    initResult
    #insert query to DB
    unless(@query.nil?)
      insertQueryToDB(@query)
      session[:query] = @query
    else
      @query = session[:query];
    end
    puts "\n## Clustering data from a search engine...\n"
    carrot = Carrot2.new
    uri = "http://localhost:8080/dcs/rest";
    @response = carrot.dcs_request(uri, {
       "dcs.source" => "images",
       "query" => @query,
       "dcs.output.format" => "JSON",
       "dcs.clusters.only" => "false"
      })

    @json = @response
    @response = JSON.parse(@response)
    #@json = @response['clusters']
    #@json = JSON.generate(@json)
    if @response['clusters'].size == 0
      flash.now[:notice] = "No cluster generated (Insufficient number of results)! "
      puts "No cluster could be generated! <Sufficient no. of result>!"
    end
    carrot.dump(@response)
    if @cluster != nil
      @clusterDocs  = @response['clusters'][@cluster]['documents']
    end
  end

  private

  def initResult
    if user_signed_in?
      @user = current_user
    end
    @country_code = get_country_code
    session[:return_to] = request.fullpath#set the place to return, if user signed then return that page
    @counter = 0
    @json = nil;
    @clusterDocs  = nil;
    @cluster = params[:clus]
    unless(params[:query].nil?)
      @query = params[:query]
    end
    @title = @query
  end

  def insertQueryToDB(query)
    #if user is not signed, just insert the query to database as anonymous user
    if(!signed_in?)
      if !Query.exists?(:query_string => query, :user_id=>nil)
        temp = Query.new(:query_string=>query, :enter_count=>1)
        temp.save
        temp.querycountries.create(:enter_count=>1, :country_code=>@country_code)
      else
        temp = Query.find(:first, :conditions=>["user_id IS NULL and query_string = :query_string",{:query_string=> query}])
        new_enter_count = temp.enter_count
        new_enter_count += 1;
        temp.update_attribute('enter_count',new_enter_count)

        country = temp.querycountries.find(:first, :conditions=>["country_code = :country_code",{:country_code=>@country_code}])
        unless country.nil?
          country.enter_count+=1
          country.save
        else
          temp.querycountries.create(:enter_count=>1, :country_code=>@country_code)
        end
      end
    return
    end
    #if user signed
    #if query exist before for that user, increment its enter_count
    if !Query.exists?(:query_string => query, :user_id=> nil)
      temp = Query.new(:query_string=>query, :enter_count=>1)
      temp.save
      temp.queries_users.create(:user_id=>current_user.id)
      temp.querycountries.create(:enter_count=>1, :country_code=>@country_code)
      #current_user.queries_users.create(temp)
    else
    #if query is new insert the query to DB eith user's id
      temp = Query.find(:first, :conditions=>["user_id IS NULL and query_string = :query_string",{:query_string=> query}])
      new_enter_count = temp.enter_count
      new_enter_count += 1;
      temp.update_attribute('enter_count',new_enter_count)
      temp.queries_users.create(:user_id=>current_user.id)

      country = temp.querycountries.find(:first, :conditions=>["country_code = :country_code",{:country_code=>@country_code}])
      unless country.nil?
        country.enter_count+=1
        country.save
      else
        temp.querycountries.create(:enter_count=>1, :country_code=>@country_code)
      end
    end
  end
end
