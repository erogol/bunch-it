class ContactController < ApplicationController
  def sendFeedback
     message = params
     FeedbackSender.new_message(message).deliver
     render :text=>"Message sent successfully!"  
  end
end