class HomeController < ApplicationController
  before_filter :authenticate_user!, :only => :token
  attr_accessor :queries
  def index
    @source_setting = "web"
    @title = "Home"
    session[:return_to] = request.fullpath
    @page = request.path
    puts "Welcome home page"
  end

  def wiki_index
    @source_setting = "wiki"
    @title = "Home"
    session[:return_to] = request.fullpath
    @page = request.path
    puts "Welcome home page"
  end

  def img_index
    @source_setting = "img"
    @title = "Home"
    session[:return_to] = request.fullpath
    @page = request.path
    puts "Welcome home page"
  end

  def job_index
    @source_setting = "job"
    @title = "Home"
    session[:return_to] = request.fullpath
    @page = request.path
    puts "Welcome home page"
  end

  def news_index
    @source_setting = "news"
    @title = "Home"
    session[:return_to] = request.fullpath
    @page = request.path
    puts "Welcome home page"
  end

  def token
    @source_setting = "web"
    @title = "Home"
    session[:return_to] = request.fullpath
    @page = request.path
    puts "Welcome home page"
  end

  def set_source_setting setting
    @source_setting = setting
  end
  
  def bubbleAnswer
      count = params[:count].to_i
     # if(@queries.nil?)
       # @queries = Query.order("updated_at DESC").limit(100)
      #end 
      render :text => Query.order('rand()').limit(1).first.query_string
  end
end
