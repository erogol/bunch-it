class UsersController < ApplicationController
  before_filter :authenticate_user!, :only => :show
  def index
    @source_setting = "web";        #set query bar to web search
    @title = current_user.user_name
    @haveQuery = false
    if @newest_query = findNewestQuery
      @common_query = commonQuery
      @haveQuery = true
    else
      @haveQuery = false
    end
  end

  def new
    @source_setting = "web";#set query bar to web search
    @title = 'Sign up'
  end

  def edit
    @source_setting = "web";#set query bar to web search
    @title = "Edit Profile"
  end

end
