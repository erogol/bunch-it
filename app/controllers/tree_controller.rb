require 'cgi'

class TreeController < ApplicationController
  before_filter :authenticate_user!
  def deleteFolder
    folder = CGI::unescapeHTML(params[:folder]);
    user = current_user.user_name
    if(folder=="null")
      render :text=>'F'
    return
    end
    #Folder.destroy_all "user_id ="+current_user.id.to_s+" and folder_name='"+folder+"'"
    folder_obj = Folder.find(:first, :conditions=>["folder_name = :folder_name and user_id = :user_id",{:folder_name=> folder, :user_id=>current_user.id}])
    folder_obj.delete
    puts "=>>>Folder "+folder+" deleted!"
    render :nothing => true
  end

  def createFolder
    folder = CGI::unescapeHTML(params[:folder]);
    id = current_user.id;
    new_folder = Folder.new(:folder_name=>folder, :user_id=>id)
    if !Folder.exists?(:folder_name=>folder, :user_id=>id)
      new_folder.save
      render :nothing=>true
    else
      render :text=>"F"
    end
  end

  def insertResult
    snippet = CGI::unescapeHTML(params[:snippet])
    url = CGI::unescapeHTML(params[:url])
    title = CGI::unescapeHTML(params[:title])
    @result = Doc.new(:title => title, :url=> url, :snippet=>snippet)
    folderName = params[:folder]
    if folderName == "null"
      folder = Folder.find(:first, :conditions=>["user_id = :user_id",{:user_id=>current_user.id}])
    else
      folder = Folder.find(:first, :conditions=>["folder_name = :folder_name and user_id = :user_id",{:folder_name=> folderName, :user_id=>current_user.id}])
    end
    @docy = folder.docs
    if nil == folder.docs.find_by_title(@result.title)
      folder.docs<<@result
      #insert_result(folderName, @result.title, @result.url)
      render :text=>folder.folder_name
    else
    #insert_result(folderName, @result.title, @result.url)
      render :text=>"F"
    end
  end

  def insert_result(folder, title, url)
    folder = CGI::unescapeHTML(folder)
    url = CGI::unescapeHTML(url)
    title = CGI::unescapeHTML(title)
    current_user.folders[:folder_name=>folder].docs.create(:title=> title, :url=> url)
  end

  def deleteResult
    title = params[:title]
    url = params[:url]
    folderName = params[:folder]
    folderName = CGI::unescapeHTML(folderName)
    url = CGI::unescapeHTML(url)
    title = CGI::unescapeHTML(title)
    if folderName == "null"
      folder = Folder.find(:first, :conditions=>["user_id = :user_id",{:user_id=>current_user.id}])
    else
      folder = Folder.find(:first, :conditions=>["folder_name = :folder_name and user_id = :user_id",{:folder_name=> folderName, :user_id=>current_user.id}])
    end
    doc = folder.docs.find(:first, :conditions=>['title = :title and url = :url',{:title=>title, :url=>url}])
    doc.destroy
    #insert_result(folderName, @result.title, @result.url)
    render :nothing=>true
  end
end
