class MapController<ApplicationController
  def getWeatherMap
    sanitizer
    lon = params[:lon]
    lat = params[:lat]
    address = "http://www.google.com/ig/api?weather=,,," + lat + "," + lon + "";
    respond = RestClient.get address
    render :xml=>respond
  end

  def getLocationMap
    sanitizer
    lon = params[:lon]
    lat = params[:lat]
    address = "http://maps.google.com/maps/api/geocode/xml?latlng=" + lat + "," + lon + "&sensor=true";
    respond = RestClient.get address
    render :xml=>respond
  end

  def getAddressMap
    sanitizer
    address = params[:address]
    address = "http://maps.googleapis.com/maps/api/geocode/json?address=" + address + "&sensor=true";
    respond = RestClient.get address
    render :xml=>respond
  end

  def sanitizer
    params do |x|
      Sanitize.clean(x)
    end
  end
end